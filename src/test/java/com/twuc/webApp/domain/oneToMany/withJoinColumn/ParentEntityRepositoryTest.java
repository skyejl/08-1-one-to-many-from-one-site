package com.twuc.webApp.domain.oneToMany.withJoinColumn;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.fail;

class ParentEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Autowired
    private ChildEntityRepository childEntityRepository;


    @Test
    void should_save_and_get_parent_and_child_entity() {
        // TODO
        //
        // 请书写如下的测试：
        //
        // Given Parent 对象和 parent 和 Child 对象 child。它们已经分别进行了持久化。
        // When 将 child 添加到 parent 的 children 集合中。并且进行持久化。
        // Then 再次查询 parent 的时候会发现 parent 的 children 集合中存在 child。
        //
        // <--start--
        ClosureValue<Long> parentId = new ClosureValue<>();
        ClosureValue<Long> childId = new ClosureValue<>();

        flushAndClear(em -> {
            ParentEntity savedParent = parentEntityRepository.save(new ParentEntity("parent"));
            ChildEntity saveChild = childEntityRepository.save(new ChildEntity("Tom"));
            saveChild.setParentEntity(savedParent);
            parentId.setValue(savedParent.getId());
            childId.setValue(saveChild.getId());
        });
        flushAndClear(em->{
            ParentEntity parentEntity = parentEntityRepository.findById(parentId.getValue()).get();
            ChildEntity childEntity = childEntityRepository.findById(childId.getValue()).get();
            parentEntity.getChildren().add(childEntity);
            parentEntityRepository.save(parentEntity);
        });
        assertEquals(1, parentEntityRepository.findById(1L).get().getChildren().size());
        // --end-->
    }

    @Test
    void should_save_parent_and_child_at_once() {
        // TODO
        //
        // 请书写如下的测试：
        //
        // Given Parent 对象和 parent 和 Child 对象 child。它们均未进行持久化。
        // When 将 child 添加到 parent 的 children 集合中。并且直接对 parent 进行持久化时。
        // Then 再次查询 parent 的时候会发现 parent 的 children 集合中存在 child。
        //
        // <--start-
        ParentEntity parentEntity = new ParentEntity();
        ChildEntity childEntity = new ChildEntity();

        flushAndClear(em-> {
            childEntity.setParentEntity(parentEntity);
            parentEntity.getChildren().add(childEntity);
            parentEntityRepository.save(parentEntity);
        });
        assertEquals(1, parentEntityRepository.findById(1L).get().getChildren().size());
        // --end-->
    }

    @Test
    void should_remove_child() {
        // TODO
        //
        // 请书写如下测试：
        //
        // Given 持久化的 parent 和 child。并且 parent 包含 child。
        // When 当使用某种方法从 parent 中移除 child 并进行持久化。
        // Then 再次查询 parent 的时候，parent 的 children 中已经不包含 child，即关联关系取消了。但是
        //   child 记录本身并未被删除。
        //
        // 在写出测试的过程中也需要考虑如何才能够正确的映射 Entity 呢？
        //
        // <--start-
        ClosureValue<Long> parentId = new ClosureValue<>();
        ClosureValue<Long> childId = new ClosureValue<>();

        flushAndClear(em -> {
            ParentEntity parentEntity = new ParentEntity("parent");
            ChildEntity childEntity = new ChildEntity("Tom");
            childEntity.setParentEntity(parentEntity);
            parentEntity.getChildren().add(childEntity);
            ChildEntity savedChild = childEntityRepository.save(childEntity);
            ParentEntity savedParent = parentEntityRepository.save(parentEntity);
            parentId.setValue(savedParent.getId());
            childId.setValue(savedChild.getId());
        });
        parentEntityRepository.findById(parentId.getValue()).get().getChildren().clear();
        ChildEntity child = childEntityRepository.findById(childId.getValue()).get();
        child.setParentEntity(null);

        assertNotNull(childEntityRepository.findById(childId.getValue()).get());
        assertEquals(0, parentEntityRepository.findById(1L).get().getChildren().size());


        // --end->
    }
}
